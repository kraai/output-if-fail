// Copyright 2018 Matt Kraai

// This file is part of output-if-fail.

// output-if-fail is free software: you can redistribute it and/or modify it under the terms of the
// GNU Affero General Public License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.

// output-if-fail is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
// the GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License along with
// output-if-fail.  If not, see <http://www.gnu.org/licenses/>.

#[macro_use]
extern crate clap;

use std::{
    io::{self, Result, Write}, os::unix::process::ExitStatusExt, process::{self, Command},
};

use clap::{App, Arg};

fn main() -> Result<()> {
    let matches = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .arg(Arg::with_name("COMMAND").required(true))
        .arg(Arg::with_name("ARGUMENT").multiple(true))
        .get_matches();
    let mut command = Command::new(matches.value_of("COMMAND").unwrap());
    if let Some(args) = matches.values_of("ARGUMENT") {
        for arg in args {
            command.arg(arg);
        }
    }
    let output = command.output()?;
    let status = output.status;
    if !status.success() {
        io::stdout().write_all(&output.stdout)?;
        io::stderr().write_all(&output.stderr)?;
        process::exit(
            status
                .code()
                .unwrap_or_else(|| status.signal().unwrap() + 128),
        );
    }
    Ok(())
}
